package lection.two.hw.one;

public class Veterinarian {
    public String name;

    public Veterinarian(String name) {
        super();
        this.name = name;
    }

    public Veterinarian() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void treatment(Animal animal) {
        System.out.println("I treat a " + animal.toString());
    }

    @Override
    public String toString() {
        return "Veterinarian{" +
                "name='" + name + '\'' +
                '}';
    }
}

