package lection.two.hw.one;

public class Main {
    
    public static void main(String[] args) {
        Cat cat = new Cat("fish", "ginger", 3, "Ginger");
        System.out.println(cat.getVoice());
        cat.eat();
        cat.sleep();
        System.out.println();

        Dog dog = new Dog("meat", "brown", 25, "Bobik");
        System.out.println(dog.getVoice());
        dog.eat();
        dog.sleep();
        System.out.println();

        Veterinarian vet1 = new Veterinarian("Petro");
        vet1.treatment(dog);
    }
}